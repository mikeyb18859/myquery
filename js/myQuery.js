/**
 * Created by mikebenn on 12/12/2016.
 */
"use strict";

var myQuery = (function () {

    function ready(callback) {
        window.onload = callback;
    }

    function find(selector) {
        var el,
            isCollection = false;


        switch (selector.charAt(0)) {
            case '.':
                el = document.getElementsByClassName(selector.substr(1));
                isCollection = true;
                break;
            case '#':
                el = document.getElementById(selector.substr(1));
                isCollection = false;
                break;
            default:

        }

        function click(callback) {
            setOneOrMore(function (e) {
                e.addEventListener('click', callback)
            });
            return retObject;
        }

        function html(html) {
            if (html) {
                setOneOrMore(function(e){e.innerHTML = html;});
                return retObject;
            } else {
                return getOneOrMore(function(e){return e.innerHTML;});
            }
        }

        function css(property, value) {

            if (typeof property === 'string') {
                if (value) {
                    setOneOrMore(function(e){e.style[property] = value});
                    return retObject;
                } else {
                    return getOneOrMore(function(e){return e.style[property]});
                }
            } else {
                for (var key in property) {
                        if (property.hasOwnProperty(key)) {
                            setOneOrMore(function(e){e.style[key] = property[key];});
                        }
                }
                return retObject;
            }
        }

        function setOneOrMore(process) {
            if (isCollection) {
                var i = 0;
                var l = el.length;
                for (i; i < l; i++) {
                    process(el[i]);
                }
            } else {
                process(el);
            }
        }
        function getOneOrMore(process){
            if (isCollection) return process(el[0]);
            else return process (el);
        }

        var retObject = {
            el: el,
            click: click,
            html: html,
            css: css
        };

        return retObject;
    }

    //PUBLIC
    return {
        ready: ready,
        find: find
    };
})();